
def AUDIT(metodo):
    def persiste(self,nome):
        print("pre-metodo")
        retorno=metodo(self,nome)
        print("pos-metodo")
        return retorno
    return persiste


class Aluno(object):
    def __init__(self):
        pass
    
    @AUDIT
    def inserir(self,nome):
        print("inserir "+nome+ " como aluno")


if __name__ == "__main__":
    Aluno().inserir("Wagner")
    Aluno().inserir("Dayana")