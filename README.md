# Design Pattern - Python
## Tipos de padrões de projetos:

1. Padrões criacionais
2. Padrões estruturais
3. Padrões comportamentais

### Strategy
O Strategy nos oferece uma maneira flexível para escrever diversos algoritmos diferentes, e de passar esses algoritmos para classes clientes que precisam deles. Esses clientes desconhecem qual é o algoritmo "real" que está sendo executado, e apenas manda o algoritmo rodar. Isso faz com que o código da classe cliente fique bastante desacoplado das implementações dos algoritmos, possibilitando assim com que esse cliente consiga trabalhar com N diferentes algoritmos sem precisar alterar o seu código.

### Chain of Responsibility
O padrão Chain of Responsibility cai como uma luva quando temos uma lista de comandos a serem executados de acordo com algum cenário em específico, e sabemos também qual o próximo cenário que deve ser validado, caso o anterior não satisfaça a condição.
Nesses casos, o Chain of Responsibility nos possibilita a separação de responsabilidades em classes pequenas e enxutas, e ainda provê uma maneira flexível e desacoplada de juntar esses comportamentos novamente

    class Desconto_por_cinco_itens(object):

        def __init__(self, proximo_desconto):
            self.__proximo_desconto = proximo_desconto

        def calcula(self, orcamento):

            if orcamento.total_itens > 5:
                return orcamento.valor * 0.1
            else: 
            return self.__proximo_desconto.calcula(orcamento)

### Template method

Quando temos diferentes algoritmos que possuem estruturas parecidas, o Template Method é uma boa solução. Com ele conseguimos definir em um nível mais macro a estrutura do algoritmo, deixando "buracos" que serão implementados de maneira diferente por cada uma das implementações específicas.

Dessa forma, reutilizamos o nosso código ao invés de repeti-lo, facilitando possíveis evoluções, tanto do algoritmo em sua estrutura macro, quanto dos detalhes do algoritmo, já que cada classe tem sua 

responsabilidade bem separada.

    class Template_de_imposto_condicional(object):

    __metaclass__ = ABCMeta

        def calcula(self, orcamento):
            if self.deve_usar_maxima_taxacao(orcamento):
                return self.maxima_taxacao(orcamento)
            else:
                return self.minima_taxacao(orcamento)

        @abstractmethod
        def deve_usar_maxima_taxacao(self, orcamento): pass

        @abstractmethod
        def maxima_taxacao(self, orcamento): pass

        @abstractmethod
        def minima_taxacao(self, orcamento): pass


### DECORATOR

Sempre que percebemos que temos comportamentos que podem ser formados por comportamentos de outras classes envolvidas em uma mesma hierarquia, como foi o caso dos impostos, que podem ser composto por outros impostos. O Decorator introduz a flexibilidade na composição desses comportamentos, bastando escolher no momento da instanciação, quais instancias serão utilizadas para realizar o trabalho.

### STAGE
A principal situação que faz emergir o Design Pattern State é a necessidade de implementação de uma máquina de estados. Geralmente, o controle das possíveis transições entre estados são várias, também são complexas, fazendo com que a implementação não seja simples. O State auxilia a manter o controle dos estados simples e organizados, através da criação de classes que representem cada estado e sabendo controlar as transições entre eles.