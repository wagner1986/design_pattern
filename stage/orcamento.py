# -*- coding: UTF-8 -*-
# orcamento.py
from estado_orcamento import Em_aprovacao
class Orcamento(object):

    def __init__(self):
        self.__itens = []
        # começa com o estado em aprovação 
        self.estado_atual = Em_aprovacao()
        self.__desconto_extra = 0

   # nova implementação de aplica_desconto_extra
    def aplica_desconto_extra(self):
        self.estado_atual.aplica_desconto_extra(self)

    def adiciona_desconto_extra(self, desconto):
        self.__desconto_extra+= desconto

    def aprova(self):
        self.estado_atual.aprova(self)

    def reprova(self):
        self.estado_atual.reprova(self)

    def finaliza(self):
        self.estado_atual.finaliza(self)

    @property
    def valor(self):
        total = 0.0
        for item in self.__itens:
            total += item.valor
        return total - self.__desconto_extra

    # retornamos uma tupla, já que não faz sentido alterar os itens de um orçamento
    def obter_itens(self):

        return tuple(self.__itens)

    # perguntamos para o orçamento o total de itens
    @property
    def total_itens(self):
        return len(self.__itens)

    def adiciona_item(self, item):
        self.__itens.append(item)

# um item criado não pode ser alterado, suas propriedades são somente leitura
class Item(object):

    def __init__(self, nome, valor):
        self.__nome = nome
        self.__valor = valor

    @property
    def valor(self):
        return self.__valor

    @property
    def nome(self):
        return self.__nome

if __name__ == '__main__':

    orcamento = Orcamento()
    orcamento.adiciona_item(Item('Item A', 100.0))
    orcamento.adiciona_item(Item('Item B', 50.0))
    orcamento.adiciona_item(Item('Item C', 400.0))

    orcamento.aplica_desconto_extra() 
    print(orcamento.valor) # imprime 522.5 porque descontou 5% de 550.0
    orcamento.aprova()

    orcamento.aplica_desconto_extra() 
    print(orcamento.valor) # imprime 512.05 porque descontou 2% de 522.5
    orcamento.finaliza()

    orcamento.aplica_desconto_extra() # lança exceção, porque não pode aplica desconto em um orçamento finalizado